'use strict'

const path = require('path')
const root = path.normalize(path.join(__dirname, './../'))
console.log(root)

const options = {
  arch: 'ia32',
  dir: root,
  platform: 'win32',
  download: {
    strictSSL: false
  },
  ignore: [
    '.gitignore',
    '.idea',
    'build',
    'test'
  ]
}

function done (err, appPaths) {
  if (err) console.log('error', err)
  console.log(appPaths)
}

const packager = require('electron-packager')
packager(options, done)
